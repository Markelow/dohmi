#include <windows.h>
#include <math.h>
#include <tchar.h>
#include "KWnd.h"
#include <wchar.h>
#include <string> 

#define IDM_REDRAW 100
#define IDM_CHOOSECOLOR 101

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void CreateControlPanel(HWND hwnd);
bool GetParams();
bool isDigit(wchar_t* buffer);
COLORREF choseColor();
void DrawPlots(HWND, HDC);

int leftBorder = 0;
int rightBorder = 100;
int thickness = 1;
HWND heLeftBorder, heRightBorder, heThickness;
COLORREF plotColor = RGB(0, 160, 0);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;
	KWnd mainWnd(L"Markelow 3100 Lab2", hInstance, nCmdShow, WndProc);	

	while (GetMessage(&msg, NULL, 0, 0))  {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hDC;
	PAINTSTRUCT ps;

	switch (uMsg)
	{
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDM_REDRAW:
					if (GetParams())
					{
						InvalidateRect(hWnd, NULL, TRUE);
					}
					break;
				case IDM_CHOOSECOLOR:
					plotColor = choseColor();
					break;
			}
			break;
		case WM_CREATE:
			CreateControlPanel(hWnd);
			break;
		case WM_PAINT:
			hDC = BeginPaint(hWnd, &ps);
			DrawPlots(hWnd, hDC);
			EndPaint(hWnd, &ps);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}

	return 0;
}

void CreateControlPanel(HWND hwnd)
{
	CreateWindowEx(WS_EX_CLIENTEDGE, L"STATIC", L"Left border:", WS_VISIBLE | WS_CHILD | SS_LEFT, 0, 5, 90, 20, hwnd, NULL, GetModuleHandle(NULL), NULL);
	heLeftBorder = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"0", WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_MULTILINE, 100, 5, 90, 20, hwnd, NULL, GetModuleHandle(NULL), NULL);
	CreateWindowEx(WS_EX_CLIENTEDGE, L"STATIC", L"Right border:", WS_VISIBLE | WS_CHILD | SS_LEFT, 0, 30, 90, 20, hwnd, NULL, GetModuleHandle(NULL), NULL);
	heRightBorder = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"100", WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_MULTILINE,100, 30, 90, 20, hwnd, NULL,GetModuleHandle(NULL), NULL);
	CreateWindowEx(WS_EX_CLIENTEDGE, L"STATIC",	L"Thickness:", WS_VISIBLE | WS_CHILD | SS_LEFT, 0, 60, 90, 20, hwnd, NULL, GetModuleHandle(NULL), NULL);
	heThickness = CreateWindowEx(WS_EX_CLIENTEDGE, L"EDIT", L"1", WS_CHILD | WS_VISIBLE | WS_TABSTOP | ES_MULTILINE, 100, 60, 90, 20, hwnd, NULL, GetModuleHandle(NULL), NULL);
	CreateWindowEx(NULL, L"BUTTON", L"Choose color", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 50, 90, 100, 24, hwnd, (HMENU)IDM_CHOOSECOLOR, GetModuleHandle(NULL), NULL);
	CreateWindowEx(NULL, L"BUTTON", L"Draw", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON, 50, 130, 100, 24, hwnd, (HMENU)IDM_REDRAW, GetModuleHandle(NULL), NULL);
}

bool GetParams()
{
	wchar_t buffer[32];
	int temp;
	SendMessage(heLeftBorder, WM_GETTEXT, sizeof(buffer) / sizeof(buffer[0]), reinterpret_cast<LPARAM>(buffer));
	if (!isDigit(buffer))
	{
		MessageBox(NULL,
			L"Incorrect left border value!",
			L"Information",
			MB_ICONERROR);
		return false;
	}
	leftBorder = _wtoi(buffer);
	SendMessage(heRightBorder, WM_GETTEXT, sizeof(buffer) / sizeof(buffer[0]), reinterpret_cast<LPARAM>(buffer));
	if (!isDigit(buffer))
	{
		MessageBox(NULL, 
			L"Incorrect right border value!",
			L"Information",
			MB_ICONERROR);
		return false;
	}
	rightBorder = _wtoi(buffer);
	if (leftBorder >= rightBorder)
	{
		MessageBox(NULL,
			L"Left border value must be less then right border value!",
			L"Information",
			MB_ICONERROR);
		leftBorder = rightBorder - 1;
		return false;
	}
	SendMessage(heThickness, WM_GETTEXT, sizeof(buffer) / sizeof(buffer[0]), reinterpret_cast<LPARAM>(buffer));
	if (!isDigit(buffer))
	{
		MessageBox(NULL,
			L"Incorrect thickness value!",
			L"Information",
			MB_ICONERROR);
		return false;
	}
	temp = _wtoi(buffer);
	if (temp <= 0)
	{
		MessageBox(NULL,
			L"Tickness value must be greater than 1!",
			L"Information",
			MB_ICONERROR);
		return false;
	}
	thickness = temp;
	return true;
}

bool isDigit(wchar_t* buffer)
{
	for (int i = 0; i < wcslen(buffer); ++i)
	{
		if (!iswdigit(buffer[i]) && !(0 == i && '-' == buffer[i]))
		{
			return false;
		}
	}
	return true;
}

COLORREF choseColor()
{
	CHOOSECOLOR color;
	COLORREF ccref[16];
	COLORREF selcolor = 0x000000;

	memset(&color, 0, sizeof(color));
	color.lStructSize = sizeof(CHOOSECOLOR);
	color.hwndOwner = NULL;
	color.lpCustColors = ccref;
	color.rgbResult = selcolor;
	color.Flags = CC_RGBINIT;

	if (ChooseColor(&color))
	{
		selcolor = color.rgbResult;
	}
	return selcolor;
}

void DrawPlots(HWND hwnd, HDC hdc)
{
	TCHAR buf[32];
	RECT rect;
	GetClientRect(hwnd, &rect);
	const int xVE = rect.right - rect.left;
	const int yVE = rect.bottom - rect.top;
	const int panelWidth = 200;
	const int plotOffset = 15;

	double maxOffset = (abs(rightBorder) > abs(leftBorder)) ? abs(rightBorder) : abs(leftBorder);
	maxOffset = (maxOffset / 10 > 1) ? maxOffset : 10;

	double nPixPerX = (xVE - panelWidth) / (2*maxOffset);  
	double nPixPerY = yVE / (2*maxOffset);

	SetMapMode (hdc, MM_ISOTROPIC);
	SetWindowExtEx (hdc, xVE, yVE, NULL);
	SetViewportExtEx (hdc, xVE, -yVE, NULL);
	SetViewportOrgEx(hdc, (xVE + panelWidth)/2, yVE / 2, NULL);

	double xpMin = -maxOffset;    
	double xpMax = maxOffset;    
	double ypMax = xpMax;   
	double ypMin = -ypMax;  
	
	int xMin = xpMin * nPixPerX;
	int xMax = xpMax * nPixPerX;


	int yMin = ypMin * nPixPerY;
	int yMax = ypMax * nPixPerY;

	double xGridStep = xpMax / 10;
	double yGridStep = ypMax / 10;
	double x, y;

	// axis x
	HPEN hPen0 = CreatePen(PS_SOLID, 3, RGB(0, 0, 0));
	HPEN hOldPen = (HPEN)SelectObject(hdc, hPen0);
   	MoveToEx(hdc, xMin+plotOffset, 0, NULL);  
	LineTo(hdc, xMax-plotOffset, 0);
	// axis y
	MoveToEx(hdc, 0, yMin+plotOffset, NULL);  
	LineTo(hdc, 0, yMax-plotOffset);

	//signature
	static LOGFONT lf;
	memset(&lf, 0, sizeof(LOGFONT));
	lf.lfPitchAndFamily = FIXED_PITCH | FF_MODERN;
	lf.lfItalic = TRUE;
	lf.lfWeight = FW_BOLD;
	lf.lfHeight = 20;
	lf.lfCharSet = DEFAULT_CHARSET;
	lstrcpy((LPWSTR)&lf.lfFaceName, L"Arial");

	HFONT hFont0 = CreateFontIndirect(&lf);
	HFONT hOldFont = (HFONT)SelectObject(hdc, hFont0);
	SetTextColor(hdc, RGB(0, 0, 200));

	TextOut(hdc, xMax - 10, 10, L"x", 1);
	TextOut(hdc, 0 - 20, yMax - 15, L"y", 1);
	// grid
	hPen0 = CreatePen(PS_SOLID, 1, RGB(0, 160, 0));
	SelectObject(hdc, hPen0);
	char vOutChar[10];
	//horizontal lines
	for (double yP = ypMin; yP < ypMax; yP += yGridStep) {
		y = yP * nPixPerY;
		MoveToEx(hdc, xMin, y, NULL);
		LineTo(hdc, xMax, y);
		_itow(yP, buf, 10);
		if (0 != yP)
		{
			TextOut(hdc, 5, y + 8, buf, wcslen(buf));
		}
	}
	//vertical lines
	for (double xP = xpMin; xP < xpMax; xP += xGridStep)
	{
		x = xP * nPixPerX;
		MoveToEx(hdc, x, yMin, NULL);
		LineTo(hdc, x, yMax);
		_itow((int)xP, buf, 10);
		TextOut(hdc, x - 6, -6, buf, wcslen(buf));
	}
	//plots
	HPEN hPen2 = CreatePen(PS_SOLID, thickness, plotColor);
	SelectObject(hdc, hPen2);
	double xStep = (double)abs(rightBorder - leftBorder)/100;     
	x = leftBorder;
	MoveToEx(hdc, x*nPixPerX, x*x*nPixPerY, NULL);
	while (x <= rightBorder)
	{
		y = x*x;
		LineTo(hdc, x * nPixPerX, y * nPixPerY);
		x += xStep;
	}

	x = (leftBorder > 0) ? leftBorder : 0;
	xStep = (double)(rightBorder) / 10000;
	MoveToEx(hdc, x*nPixPerX, x*x*nPixPerY, NULL);
	while (x <= rightBorder)
	{
		y = sqrt(x);
		LineTo(hdc, x * nPixPerX, y * nPixPerY);
		x += xStep;
	}
	SelectObject(hdc, hOldPen);
	SelectObject(hdc, hOldFont);
}