#include <windows.h>
#include <math.h>
#include <tchar.h>
#include "KWnd.h"
#include <wchar.h>
#include <string>
#include <thread>
#include <commctrl.h>

#pragma comment(lib, "comctl32.lib")

#define ID_TRACKBAR 10000
#define PI 3.141592
#define radius 50
#define MAX_SPEED 10

COLORREF startColor;
POINT center;
const int step = 10;
DWORD speed = 1;

bool stop = true;
std::thread *thr;
HWND hWin, trackBar;
HINSTANCE gHinstance;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
COLORREF ChoseColor();
HWND WINAPI CreateTrackbar(HWND , UINT, UINT, UINT, UINT); 

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;
	KWnd mainWnd(L"Markelow 3100 Lab4", hInstance, nCmdShow, WndProc);
	gHinstance = hInstance;
	while (GetMessage(&msg, NULL, 0, 0))  {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hDC;
	PAINTSTRUCT ps;
	switch (uMsg)
	{
	case WM_CREATE:
		startColor = ChoseColor();
		center.x = 150;
		center.y = 150;
		hWin = hWnd;
		trackBar = CreateTrackbar(hWnd, 1, 10, 1, 10);
		break;
	case WM_HSCROLL:
	{
		speed = SendMessage(trackBar, TBM_GETPOS, 0, 0);
		SetFocus(hWnd);
		break;
	}
	case WM_PAINT:
	{
		hDC = BeginPaint(hWnd, &ps);
		SelectObject(hDC, CreatePen(PS_SOLID, 1, startColor));
		const double rotation = 22.5;
		int factor;
		static POINT points[17];
		for (int i = 0; i < 17; ++i)
		{
			factor = (0 == i % 2) ? radius : radius - 20;
			points[i].x = center.x + factor * sin(rotation * i *PI / 180);
			points[i].y = center.y - factor * cos(rotation * i*PI / 180);
		}
		Polyline(hDC, points, sizeof(points) / sizeof(POINT));

		EndPaint(hWnd, &ps);
		break;
	}
	case WM_KEYDOWN:
	{
		if (VK_RIGHT != wParam && VK_LEFT != wParam)
		{
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
		}
		if (nullptr != thr)
		{
			stop = true;
			thr->join();
			delete thr;
		}
		stop = false;
		thr = new std::thread([wParam]()
		{
			for (;;)
			{
				
				std::chrono::milliseconds sleep_time(30 / speed * MAX_SPEED);
				RECT lp;
				GetClientRect(hWin, &lp);
				if (VK_RIGHT == wParam)
				{
					if (lp.right > center.x + step + radius)
					{
						center.x = center.x + step ;
					}
					else
					{
						break;
					}
				}
				if (VK_LEFT == wParam)
				{
					if (lp.left < center.x - radius - step)
					{
						center.x =  center.x - step ;
					}
					else
					{
						break;
					}
				}
				std::this_thread::sleep_for(sleep_time);
				InvalidateRect(hWin, nullptr, true); 
				if (stop) { return; }
			}
		});
		break;
	}
	case WM_GETMINMAXINFO:
	{
		MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(lParam);
		mmi->ptMinTrackSize.x = 300;
		mmi->ptMinTrackSize.y = 200;
		break;
	}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

COLORREF ChoseColor()
{
	CHOOSECOLOR color;
	COLORREF ccref[16];
	COLORREF selcolor = 0x000000;

	memset(&color, 0, sizeof(color));
	color.lStructSize = sizeof(CHOOSECOLOR);
	color.hwndOwner = NULL;
	color.lpCustColors = ccref;
	color.rgbResult = selcolor;
	color.Flags = CC_RGBINIT;

	if (ChooseColor(&color))
	{
		selcolor = color.rgbResult;
	}
	return selcolor;
}

HWND WINAPI CreateTrackbar(
	HWND hwndDlg,  // handle of dialog box (parent window) 
	UINT iMin,     // minimum value in trackbar range 
	UINT iMax,     // maximum value in trackbar range 
	UINT iSelMin,  // minimum value in trackbar selection 
	UINT iSelMax)  // maximum value in trackbar selection 
{

	InitCommonControls(); // loads common control's DLL 

	HWND hwndTrack = CreateWindowEx(
		0,                               // no extended styles 
		TRACKBAR_CLASS,                  // class name 
		L"Trackbar Control",              // title (caption) 
		WS_CHILD |
		WS_VISIBLE |
		TBS_AUTOTICKS |
		TBS_ENABLESELRANGE,              // style 
		20, 20,                          // position 
		200, 30,                         // size 
		hwndDlg,                         // parent window 
		reinterpret_cast<HMENU__*>(ID_TRACKBAR),                     // control identifier 
		gHinstance,                         // instance 
		NULL                             // no WM_CREATE parameter 
		);

	SendMessage(hwndTrack, TBM_SETRANGE,
		(WPARAM)TRUE,                   // redraw flag 
		(LPARAM)MAKELONG(iMin, iMax));  // min. & max. positions

	SendMessage(hwndTrack, TBM_SETPAGESIZE,
		0, (LPARAM)4);                  // new page size 

	SendMessage(hwndTrack, TBM_SETSEL,
		(WPARAM)FALSE,                  // redraw flag 
		(LPARAM)MAKELONG(iSelMin, iSelMax));

	SendMessage(hwndTrack, TBM_SETPOS,
		(WPARAM)TRUE,                   // redraw flag 
		(LPARAM)iSelMin);

	SetFocus(hwndTrack);

	return hwndTrack;
}
