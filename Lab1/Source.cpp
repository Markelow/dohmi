#include <windows.h>
#include <math.h>
#include "resource.h"

#define VK_ONE 0x31 
#define VK_TWO 0x32 
#define CAT_BITMAP IDB_BITMAP3
#define CLOUD_BITMAP IDB_BITMAP2
#define WATERMARK_BITMAP IDB_BITMAP4
#define BLACK_PIXEL RGB(0,0,0)

HINSTANCE hInst;
HBITMAP backgroundImage = NULL;
HBITMAP waterMarkImage = NULL;
HDC hdcMemWaterMark;
BOOL isDrawingWaterMark;

const int wndWidth = 710;
const int wndHeight = 730;
const int waterMarkSize = 50;
const int rectangleSIze = (int)(waterMarkSize*(sqrt((double)2) / 2));
const int offset = (waterMarkSize - rectangleSIze)/2;
const double factor = 1.9;

ATOM WINAPI InitializeWindow(TCHAR className[], HINSTANCE hInst);
LRESULT CALLBACK WindowProcess(HWND, UINT, WPARAM, LPARAM);
void LoadImage(HWND hWindow, int imageId, HBITMAP* bitmap);
void DrawWaterMark(HDC* hdc, int width, int height);
void UpdatePixel(HDC* hdc, int x, int y); 

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR pCommandLine, int nCommandShow)
{
	TCHAR className[] = L"WINAPI32";
	HWND hWindow;
	MSG message;
	hInst = hInstance;
	if (!InitializeWindow(className, hInst))
	{
		MessageBox(NULL, L"Can not register window", L"Error", MB_OK);
		return NULL;
	}
	hWindow = CreateWindow(className, L"Markelow 3100 Lab1", WS_OVERLAPPED, CW_USEDEFAULT, NULL, wndWidth, wndHeight, (HWND)NULL, NULL, HINSTANCE(hInst), NULL);
	if (!hWindow)
	{
		MessageBox(NULL, L"Can not create window", L"Error", MB_OK);
		return NULL;
	}
	ShowWindow(hWindow, nCommandShow);
	UpdateWindow(hWindow);
	while (GetMessage(&message, NULL, NULL, NULL))
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
	}
	return message.wParam;
}

ATOM WINAPI InitializeWindow(TCHAR className[], HINSTANCE hInst)
{
	WNDCLASSEX windowClass;
	windowClass.cbSize = sizeof(windowClass);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = WindowProcess;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = className;
	windowClass.cbWndExtra = NULL;
	windowClass.cbClsExtra = NULL;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);;
	windowClass.hInstance = hInst;
	return RegisterClassEx(&windowClass);
}

LRESULT CALLBACK WindowProcess(HWND hWindow, UINT uMessage, WPARAM wParameter, LPARAM lParameter)
{
	BITMAP bm;
	PAINTSTRUCT ps;
	HDC hdc, hdcMem;
	int width, height;
	switch (uMessage)
	{
		case WM_CREATE:
			LoadImage(hWindow, CLOUD_BITMAP, &backgroundImage);
			LoadImage(hWindow, WATERMARK_BITMAP, &waterMarkImage);
			hdcMemWaterMark = CreateCompatibleDC(NULL);
			GetObject(waterMarkImage, sizeof(bm), &bm);
			SelectObject(hdcMemWaterMark, waterMarkImage);
			StretchBlt(hdcMemWaterMark, 0, 0, waterMarkSize, waterMarkSize, hdcMemWaterMark, 0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);
			isDrawingWaterMark = false;
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWindow, &ps);
			hdcMem = CreateCompatibleDC(hdc);
			GetObject(backgroundImage, sizeof(bm), &bm);
			SelectObject(hdcMem, backgroundImage);
			width = bm.bmWidth;
			height = bm.bmHeight;
			BitBlt(hdc, 0, 0, width, height, hdcMem, 0, 0, SRCCOPY);			
			if (isDrawingWaterMark)
			{
				DrawWaterMark(&hdc, width, height);
			}
			DeleteDC(hdcMem);
			EndPaint(hWindow, &ps);
			break;
		case WM_KEYDOWN:
			switch (wParameter)
			{
				case VK_RETURN:
					isDrawingWaterMark = (IDYES == MessageBox(NULL, L"Do you want some magic?", L"Magic", MB_YESNO));
					InvalidateRect(hWindow, NULL, true);
					break;
				case VK_ONE:
					LoadImage(hWindow, CLOUD_BITMAP, &backgroundImage);
					InvalidateRect(hWindow, NULL, true);
					break;
				case VK_TWO:
					if (IDYES == MessageBox(NULL, L"Do you love cats?", L"Question", MB_YESNO))
					{
						LoadImage(hWindow, CAT_BITMAP, &backgroundImage);
						InvalidateRect(hWindow, NULL, true);
					}
					break;
				default:
					break;
			}
			break;
		case WM_DESTROY:
			DeleteDC(hdcMemWaterMark);
			DeleteObject(waterMarkImage);
			DeleteObject(backgroundImage);
			PostQuitMessage(EXIT_SUCCESS);
			break;
		default:
			return DefWindowProc(hWindow, uMessage, wParameter, lParameter);
	}
	return NULL;
}	

void LoadImage(HWND hWindow, int imageId, HBITMAP* bitmap)
{
	*bitmap = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(imageId));
	if (NULL == *bitmap)
	{
		MessageBox(hWindow, L"Could not load image!", L"Error", MB_OK | MB_ICONEXCLAMATION);
		PostQuitMessage(EXIT_FAILURE);
	}
}

void DrawWaterMark(HDC* hdc, int width, int height)
{
	for (int x = 0; x < waterMarkSize; ++x)
	{
		for (int y = 0; y < waterMarkSize; ++y)
		{
			if (BLACK_PIXEL == GetPixel(hdcMemWaterMark, x, y))
			{
				UpdatePixel(hdc, width - waterMarkSize + x, height - waterMarkSize + y);
			}

		}
	}
}

void UpdatePixel(HDC* hdc, int x, int y)
{
	COLORREF pixel;
	pixel = GetPixel(*hdc, x, y);
	pixel = RGB(GetRValue(pixel), GetGValue(pixel)*factor, GetBValue(pixel));
	SetPixel(*hdc, x, y, pixel);
}