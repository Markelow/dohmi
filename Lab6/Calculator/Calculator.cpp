#include "Calculator.h"

Calculator::Calculator()
{
	currentOperand = Fraction(0, 1);
	previosOperand = Fraction(0, 1);
	memory = Fraction(0, 1);
	operation = nopeOp;
	isCompleted = false;
	isSaved = false;
}

//��������, ��������� �� ���������� ���������
bool Calculator::IsCompleted()
{
	return isCompleted;
}

//���������� ������� �������
Fraction Calculator::getCurrentOperand()
{
	return currentOperand;
}

//������������� ����� �������
void Calculator::setCurrentOperand(Fraction value)
{
	//��������� ������ ������� ������� � ����������
	previosOperand = currentOperand;
	currentOperand = value;
}

//���������� ���������� �������
Fraction Calculator::getPreviosOperand()
{
	return previosOperand;
}

//���������� ��������� �������� ��������
Fraction Calculator::Add()
{
	return Operate(addOp);
}

//��������� ��������� ������� ��������
Fraction Calculator::Operate(Operation value)
{
	Fraction result; 
	if (!isCompleted)
	{
		//���� ��� �� ��� ���������, �� �������
		result = PartialComplete();
	}
	else
	{
		//����� ���������� ������� �������
		result = currentOperand;
	}
	//���������� ��������, ������� ����� ��������� � ��������� ���
	operation = value;
	isCompleted = false;
	isSaved = false;
	return result;
}

//��������� �������� ��������� ��������� 
Fraction Calculator::Complete()
{
	isCompleted = true;
	return PartialComplete();	
}

//��������� ���������� ��������
Fraction Calculator::PartialComplete()
{
	Fraction result;
	switch (operation)
	{
		case addOp:
			result = currentOperand + previosOperand;
			if (isCompleted && !isSaved) { previosOperand = currentOperand; isSaved = true; }
			currentOperand = result;
			return result;
		case subOp:
			result = (isCompleted) ? currentOperand - previosOperand : previosOperand - currentOperand;
			if (isCompleted && !isSaved) { previosOperand = currentOperand; isSaved = true; }
			currentOperand = result;
			return result;
		case multOp:
			result = currentOperand * previosOperand;
			currentOperand = result;
			return result;
		case divOp:
			result = (isCompleted) ? currentOperand / previosOperand : previosOperand / currentOperand;
			if (isCompleted && !isSaved) { previosOperand = currentOperand; isSaved = true; }
			currentOperand = result;
			return result;
		default:
			previosOperand = currentOperand;
			return currentOperand;
	}
}

//���������� ��������� ���������
Fraction Calculator::Subtsract()
{
	return Operate(subOp);
}

//���������� ��������� ���������
Fraction Calculator::Miltiple()
{
	return Operate(multOp);
}
//���������� ��������� �������
Fraction Calculator::Divide()
{
	return Operate(divOp);
}

//������������� �����������
void Calculator::Reset()
{
	*this = Calculator();
}

//��������� ����� � �������� � ������
void Calculator::MemoryAdd(Fraction fraction)
{
	memory = memory + fraction;
}

//������� ������
void Calculator::MemoryClear()
{
	memory = Fraction(0, 1);
}

//�������������� ����������� ��������
Fraction Calculator::MemoryRestore()
{
	currentOperand = memory;
	return memory;
}

//������� ����� � ������
void Calculator::MemorySave(Fraction fraction)
{
	memory = fraction;
}

//��������� ������� �� 0 � ������
bool Calculator::IsMemoryOn()
{
	return !memory.IsZero();
}