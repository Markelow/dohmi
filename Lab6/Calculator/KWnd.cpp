#include "KWnd.h"
#include <tchar.h>

KWnd::KWnd(LPCTSTR windowName, wchar_t* ClassName, HINSTANCE hInst, int cmdShow,
	LRESULT(WINAPI *pWndProc)(HWND, UINT, WPARAM, LPARAM),
	LPCTSTR menuName, int x, int y, int width, int height,
	UINT classStyle, DWORD windowStyle, HWND hParent)
{

	wc.cbSize = sizeof(wc);
	wc.style = classStyle;
	wc.lpfnWndProc = pWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInst;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = CreateSolidBrush(RGB(255,0,0));
	wc.lpszMenuName = menuName;
	wc.lpszClassName = ClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// ������������ ����� ����
	if (!RegisterClassEx(&wc)) {
		/*wchar_t msg[100] = L"Cannot register class: ";
		wcscat(msg, ClassName);
		MessageBox(NULL, msg, L"Error", MB_OK);
		return;*/
	}

	// c������ ����
	hWnd = CreateWindow(ClassName, windowName, windowStyle,
		x, y, width, height, hParent, (HMENU)NULL, hInst, NULL);

	if (!hWnd) {
		wchar_t text[100] = L"Cannot create window: ";
		wcscat(text, windowName);
		MessageBox(NULL, text, L"Error", MB_OK);
		return;
	}

	// ����������  ����
	ShowWindow(hWnd, cmdShow);
}