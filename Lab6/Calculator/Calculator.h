#ifndef CALCULATOR_H
#define  CALCULATOR_H

#include "Enums.h"
#include "Fraction.h"

class Calculator
{
	public:
		Calculator();
		Fraction getCurrentOperand();
		void setCurrentOperand(Fraction);
		Fraction getPreviosOperand();
		Fraction Complete();
		Fraction PartialComplete();
		Fraction Add();
		Fraction Subtsract();
		Fraction Miltiple();
		Fraction Divide();
		void Reset();
		void MemoryClear();
		Fraction MemoryRestore();
		void MemorySave(Fraction);
		void MemoryAdd(Fraction);
		bool IsMemoryOn();
		bool IsCompleted();

	protected:
		Fraction Operate(Operation);
		Fraction currentOperand;
		Fraction previosOperand;
		Fraction memory;
		Operation operation;
		bool isCompleted;
		bool isSaved;
};

#endif