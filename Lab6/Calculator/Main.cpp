#include <windows.h>
#include <tchar.h>
#include "KWnd.h"
#include <wchar.h>
#include <string>
#include "Fraction.h"
#include "Calculator.h"
#include "resource.h"
#include "CalcErrors.h"
#include <commctrl.h>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "comctl32.lib")

using namespace std;

//������ ��������� �������
#define MA_COPY 1
#define MA_PASTE 2
#define MA_MODE_INTEGER 3
#define MA_MODE_FRACTION 4
#define MA_DELIM_STRAIGHT 5
#define MA_DELIM_SLOOPING 6
#define MA_HELP 7

//������ ��������� �� ������� ������
#define IDM_0 100
#define IDM_1 101
#define IDM_2 102
#define IDM_3 103
#define IDM_4 104
#define IDM_5 105
#define IDM_6 106
#define IDM_7 107
#define IDM_8 108
#define IDM_9 109
#define IDM_ADD 110
#define IDM_SUBTRACT 111
#define IDM_MULTIPLY 112
#define IDM_DIVIDE 113
#define IDM_SQR 114
#define IDM_REV 115
#define IDM_COMPLETE 116
#define IDM_SEPARATE 117
#define IDM_INVERSE 118
#define IDM_C 119
#define IDM_CE 120
#define IDM_BACKSPASE 121
#define IDM_MC 122
#define IDM_MR 123
#define IDM_MS 124
#define IDM_MA 125

//����������� ���� �������� ������ � ������ ��������������
#define VK_ZERO 0x30
#define VK_ONE 0x31
#define VK_TWO 0x32
#define VK_THREE 0x33
#define VK_FOUR 0x34
#define VK_FIVE 0x35
#define VK_SIX 0x36
#define VK_SEVEN 0x37
#define VK_EIGHT 0x38
#define VK_NINE 0x39
#define VK_C 0x043
#define VK_V 0x56

HWND hMemoryStatus, hEdit;
Calculator calculator;
wstring display;
bool isValueEntered;
HMENU hMConfigMode, hMConfigDelimiter;
HINSTANCE gHInstance;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

void ProccesKeys(int VK, HWND hwnd);
bool IsDigitKeys(int VK);
void ProccessDigitKeys(int, HWND);
void UpdateDisplay(HWND hwnd);
void UpdareMemoryDisplay(HWND hwnd, bool isMemoryOn);
void MemoryAdd(HWND hwnd);
void MemorySave(HWND hwnd);
void Backspace(HWND hwnd);
void Separate(HWND);
void Add(HWND hwnd); 
void Operate(HWND hwnd, Operation operation);
void Complete(HWND hwnd);
void ErrorMessage(HWND hwnd, int exCode);
void Substract(HWND hwnd); 
void Mulitple(HWND hwnd);
void Divide(HWND hwnd); 
void Sqr(HWND hwnd);
void Rev(HWND hwnd);
void Copy(HWND hwnd);
void Paste(HWND hwnd);
void CreateControlPanel(HWND hwnd);
void CreateToolTipForRect(HWND hwndParent, LPWSTR message);
void CreateMenus(HWND hwnd); 
HWND CreateButton(HWND hwnd, LPCWSTR name, int x, int y, int width, int height, HMENU hmenuID);
void ShowHelp(HWND hwnd);
LRESULT CALLBACK AboutWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	gHInstance = hInstance;
	MSG msg;
	KWnd mainWnd(L"����������� ���������", L"MainWindow", hInstance, nCmdShow, WndProc, NULL, 0, 0, 480, 340, NULL, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX );


	
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_CREATE:
		{
			// ����������� ������������
			isValueEntered = true;
			display = L"0";
			calculator = Calculator();
			Fraction::setSeparator(slopingSM);
			Fraction::setMode(integerFM);
			// ���������� ���� � ������������ (��������� ���������)
			SetWindowLong(hWnd, GWL_EXSTYLE, GetWindowLong(hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
			SetLayeredWindowAttributes(hWnd, RGB(255, 0, 0), 0, LWA_COLORKEY);
			
			//������� ���� � �������� 
			CreateMenus(hWnd);
			CreateControlPanel(hWnd);
			
			//��������� ������� ��������� �����������
			CheckMenuItem(hMConfigDelimiter, MA_DELIM_SLOOPING, MF_CHECKED);
			CheckMenuItem(hMConfigMode, MA_MODE_INTEGER, MF_CHECKED);
			
			// ����� ���� �������
			WNDCLASSEX wc;
			wc.cbSize = sizeof(wc);
			wc.style = CS_HREDRAW | CS_VREDRAW;
			wc.lpfnWndProc = AboutWndProc;
			wc.cbClsExtra = 0;
			wc.cbWndExtra = 0;
			wc.hInstance = gHInstance;
			wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			wc.hCursor = LoadCursor(NULL, IDC_ARROW);
			wc.hbrBackground = CreateSolidBrush(RGB(255, 0, 0));
			wc.lpszMenuName = NULL;
			wc.lpszClassName = L"aboutWindow";
			wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

			// ������������ ����� ���� �������
			if (!RegisterClassEx(&wc)) {
				wchar_t msg[100] = L"Cannot register class: ";
				wcscat(msg, L"aboutWindow");
				MessageBox(NULL, msg, L"Error", MB_OK);
			}
			break;
		}	
		// ��������� ������
		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case IDM_BACKSPASE:
					Backspace(hWnd);
					break;
				case IDM_C:
					calculator.Reset();
					UpdareMemoryDisplay(hWnd, false);
				case IDM_CE:
					display = L"0";
					UpdateDisplay(hWnd);
					break;
				case IDM_0:
				case IDM_1:
				case IDM_2:
				case IDM_3:
				case IDM_4:
				case IDM_5:
				case IDM_6:
				case IDM_7:
				case IDM_8:
				case IDM_9:
					ProccessDigitKeys(LOWORD(wParam) - 4, hWnd);
					break;
				case IDM_SEPARATE:
					Separate(hWnd);
					break;
				case IDM_INVERSE:
					if (1 <= display.length())
					{
						if (L'-' == display[0])
						{
							display.erase(0, 1);
						}
						else
						{
							display = L"-" + display;
						}
						UpdateDisplay(hWnd);
					}
					break;
				case IDM_COMPLETE:
					Complete(hWnd);
					break;
				case IDM_ADD:
					Add(hWnd);
					break;
				case IDM_SUBTRACT:
					Substract(hWnd);
					break;
				case IDM_MULTIPLY:
					Mulitple(hWnd);
					break;
				case IDM_DIVIDE:
					Divide(hWnd);
					break;
				case IDM_SQR:
					Sqr(hWnd);
					break;
				case IDM_REV:
					Rev(hWnd);
					break;
				case IDM_MC:
					calculator.MemoryClear();
					UpdareMemoryDisplay(hWnd, false);
					break;
				case IDM_MA:
					MemoryAdd(hWnd);
					break;
				case IDM_MS:
					MemorySave(hWnd);
					break;
				case IDM_MR:
					if (!calculator.MemoryRestore().IsZero())
					{
						display = calculator.MemoryRestore().ToString();
						UpdateDisplay(hWnd);
					}
					break;
				case MA_MODE_FRACTION:
					//����� ����������� �����
					Fraction::setMode(fractionFM);
					CheckMenuItem(hMConfigMode, MA_MODE_FRACTION, MF_CHECKED);
					CheckMenuItem(hMConfigMode, MA_MODE_INTEGER, MF_UNCHECKED);
					display = Fraction(display).ToString();
					UpdateDisplay(hWnd);
					break;
				case MA_MODE_INTEGER:
					//����� ����������� �����
					Fraction::setMode(integerFM);
					CheckMenuItem(hMConfigMode, MA_MODE_FRACTION, MF_UNCHECKED);
					CheckMenuItem(hMConfigMode, MA_MODE_INTEGER, MF_CHECKED);
					display = Fraction(display).ToString();
					UpdateDisplay(hWnd);
					break;
				case MA_DELIM_SLOOPING:
				{
					// ��������� ����������� /
					Fraction::setSeparator(slopingSM);
					CheckMenuItem(hMConfigDelimiter, MA_DELIM_SLOOPING, MF_CHECKED);
					CheckMenuItem(hMConfigDelimiter, MA_DELIM_STRAIGHT, MF_UNCHECKED);
					display = Fraction(display).ToString();
					UpdateDisplay(hWnd);
					break;
				}
				case MA_DELIM_STRAIGHT:
					// ������ ����������� |
					Fraction::setSeparator(striaghtSM);
					CheckMenuItem(hMConfigDelimiter, MA_DELIM_SLOOPING, MF_UNCHECKED);
					CheckMenuItem(hMConfigDelimiter, MA_DELIM_STRAIGHT, MF_CHECKED);
					display = Fraction(display).ToString();
					UpdateDisplay(hWnd);
					break;
				case MA_COPY:
					Copy(hWnd);
					break;
				case MA_PASTE:
					Paste(hWnd);
					break;
				case MA_HELP:
					ShowHelp(hWnd);
					break;
			}
			SetFocus(hWnd);
			break;
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(lParam);
			mmi->ptMinTrackSize.x = 480;
			mmi->ptMinTrackSize.y = 450	;
			break;
		}
		case WM_CTLCOLORSTATIC:
		{
			//������ ��� ����������
			HDC hdcStatic = (HDC)wParam;
			SetBkMode(hdcStatic, TRANSPARENT);
			return (INT_PTR)GetStockObject(NULL_BRUSH);
		}
		case WM_KEYDOWN:
			ProccesKeys(wParam, hWnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

//������������ ������� �������
void ProccesKeys(int VK, HWND hwnd)
{

	if (IsDigitKeys(VK))
	{
		ProccessDigitKeys(VK, hwnd);
	}
	else
	{
		switch (VK)
		{
			case VK_BACK:
				Backspace(hwnd);
				break;
			case VK_DECIMAL:
				Separate(hwnd);
				break;
			case VK_ADD:
				Add(hwnd);
				break;
			case VK_SUBTRACT:
				Substract(hwnd);
				break;
			case VK_MULTIPLY:
				Mulitple(hwnd);
				break;
			case VK_DIVIDE:
				Divide(hwnd);
				break;
			case VK_RETURN:
				Complete(hwnd);
				break;
			case VK_ESCAPE:
				calculator.Reset();
			case VK_DELETE:
				display = L"0";
				UpdateDisplay(hwnd);
				break;
			case VK_C:
				if (0 != (GetKeyState(VK_CONTROL) & 0x8000))
				{
					Copy(hwnd);
				}
				break;
			case VK_V:
				if (0 != (GetKeyState(VK_CONTROL) & 0x8000))
				{
					Paste(hwnd);
				}
				break;
		default:
			break;
		}
	}
}

//���������, �������� �� ������ ������
bool IsDigitKeys(int VK)
{
	return (0x60 <= VK && 0x69 >= VK || 0x30 <= VK && 0x39 >= VK);
}

//����������� �������� ������
void ProccessDigitKeys(int VK, HWND hwnd)
{
	if (0 == display.compare(L"0"))
	{
		display = L"";
	}
	if (!isValueEntered)
	{
		display = L"";
	}
	isValueEntered = true;
	switch (VK)
	{
		case VK_ZERO:
		case VK_NUMPAD0:
			display.append( L"0");
			break;
		case VK_ONE:
		case VK_NUMPAD1:
			display.append(L"1");
			break;
		case VK_TWO:
		case VK_NUMPAD2:
			display.append(L"2");
			break;
		case VK_THREE:
		case VK_NUMPAD3:
			display.append(L"3");
			break;
		case VK_FOUR:
		case VK_NUMPAD4:
			display.append(L"4");
			break;
		case VK_FIVE:
		case VK_NUMPAD5:
			display.append(L"5");
			break;
		case VK_SIX:
		case VK_NUMPAD6:
			display.append(L"6");
			break;
		case VK_SEVEN:
		case VK_NUMPAD7:
			display.append(L"7");
			break;
		case VK_EIGHT:
		case VK_NUMPAD8:
			display.append(L"8");
			break;
		case VK_NINE:
		case VK_NUMPAD9:
			display.append(L"9");
			break;
		default:
			break;
	}
	UpdateDisplay(hwnd);
}

//��������� ���� �� ������� � ������
void UpdateDisplay(HWND hwnd)
{
	SetWindowText(hEdit, display.c_str());
	RECT rect = { 10, 5, 470, 50, };
	InvalidateRect(hwnd, &rect, TRUE);
	UpdateWindow(hwnd);
}

//��������� ������������ ������� � ������
void MemoryAdd(HWND hwnd)
{
	try
	{
		Fraction fraction(display);
		if (!fraction.IsZero())
		{
			calculator.MemoryAdd(fraction);
			UpdareMemoryDisplay(hwnd, true);
		}
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//������� ������������ ������� � ������
void MemorySave(HWND hwnd)
{
	try
	{
		Fraction fraction(display);
		if (!fraction.IsZero())
		{
			calculator.MemorySave(fraction);
			UpdareMemoryDisplay(hwnd, true);
		}
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//���������� ������� �������� ������
void UpdareMemoryDisplay(HWND hwnd, bool isMemoryOn)
{
	wchar_t* status = (isMemoryOn )? L"M" : L" ";
	SetWindowText(hMemoryStatus, status);
	RECT rect = { 10, 80, 60, 50, };
	GetClientRect(hMemoryStatus, &rect);
	InvalidateRect(hMemoryStatus, &rect, TRUE);
	MapWindowPoints(hMemoryStatus, hwnd, (POINT *)&rect, 2);
	RedrawWindow(hwnd, &rect, NULL, RDW_ERASE | RDW_INVALIDATE);
}

//������������ ������� "������� ������"
void Backspace(HWND hwnd)
{
	switch (display.length())
	{
		case 0:
		case 1:
			display =  L"0";
			break;
		case 2:
			if (L'-' == display[1])
			{
				display =  L"0";
			}
		default:
			display.pop_back();
			break;
		}
	UpdateDisplay(hwnd);
}

//������������ ������� "�������� ����������� �����"
void Separate(HWND hwnd)
{
	//���� ��� ��� �����������
	if (-1 == display.find(Fraction::getDelimiter()))
	{
			display += Fraction::getDelimiter();
			UpdateDisplay(hwnd);
	}
}

//���������� ������������ ������� � ���, ������� ��� ������� � �����������
void Add(HWND hwnd)
{
	Operate(hwnd, addOp);
}

//��������� ��������� ��������
void Operate(HWND hwnd, Operation operation)
{
	try
	{
		//���� �������� ���� ������� ������������� ��� ���������� ��������� ���������
		if (isValueEntered || calculator.IsCompleted())
		{
			//����� ��� ����, ����� ������ ����� ����������
			if (isValueEntered && calculator.IsCompleted())
			{
				calculator.setCurrentOperand(Fraction(0, 1));
			}
			//��������� ������� �������
			calculator.setCurrentOperand(Fraction(display));
		}
		//��������� ���������� ���������� � ������� ��������� 
		switch (operation)
		{
			case addOp:
				display = calculator.Add().ToString();
				break;
			case subOp:
				display = calculator.Subtsract().ToString();
				break;
			case divOp:
				display = calculator.Divide().ToString();
				break;
			case multOp:
				display = calculator.Miltiple().ToString();
				break;
			default:
				throw 0;
		}
		UpdateDisplay(hwnd);
		isValueEntered = false;
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//��������� ���������� ���������
void Complete(HWND hwnd)
{
	try
	{	
		if (isValueEntered)
		{
			calculator.setCurrentOperand(Fraction(display));
		}
		display = calculator.Complete().ToString();
		UpdateDisplay(hwnd);
		isValueEntered = false;
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//������� ��������� �� ������ �� ������ ���� ������
void ErrorMessage(HWND hwnd, int exCode)
{
	wstring errorMessage;
	switch (exCode)
	{
		case 91:
			errorMessage = ER91;
			break;
		case 92:
			errorMessage = ER92;
			break;
		case 93:
			errorMessage = ER93;
			break;
		case 97:
			errorMessage = ER97;
			break;
		default:
			errorMessage = L"�������������� ����������.";
			break;
	}
	MessageBox(hwnd, errorMessage.c_str(), L"������", MB_ICONERROR);
}

//��������� �������� ���������
void Substract(HWND hwnd)
{
	Operate(hwnd, subOp);
}

//��������� �������� ���������
void Mulitple(HWND hwnd)
{

	Operate(hwnd, multOp);
}

//�������� �������� �������
void Divide(HWND hwnd)
{
	Operate(hwnd, divOp);
}

//�������� � ������� ������������ �������
void Sqr(HWND hwnd)
{
	try
	{
		display = Fraction(display).Sqr().ToString();
		UpdateDisplay(hwnd);
		isValueEntered = true;
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//������� �����, �������� ������������
void Rev(HWND hwnd)
{
	try
	{
		display = Fraction(display).Rev().ToString();
		UpdateDisplay(hwnd);
		isValueEntered = true;
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//�������� ������������ ����� � ������
void Copy(HWND hwnd)
{
	try
	{
		OpenClipboard(0);
		EmptyClipboard();
		HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, (1 + display.size())*sizeof(wchar_t));
		if (!hg)
		{
			CloseClipboard();
			return;
		}
		memcpy(GlobalLock(hg), display.c_str(), (1 + display.size())*sizeof(wchar_t));
		GlobalUnlock(hg);
		SetClipboardData(CF_UNICODETEXT, hg);
		CloseClipboard();
		GlobalFree(hg);
	}
	catch (int exCode)
	{
		ErrorMessage(hwnd, exCode);
	}
}

//��������� ����� �� ������
void Paste(HWND hwnd)
{
	try
	{
		if (OpenClipboard(0))
		{
			if (IsClipboardFormatAvailable(CF_UNICODETEXT))
			{
				HANDLE hClipboardData = GetClipboardData(CF_UNICODETEXT);
				wchar_t *temp = (wchar_t*)GlobalLock(hClipboardData);
				wstring fraction = temp;
				display = Fraction(fraction).ToString();
				UpdateDisplay(hwnd);
				GlobalUnlock(hClipboardData);
			}
		}
	}
	catch (int exCode)
	{
		if (OpenClipboard(0))
		{
			if (IsClipboardFormatAvailable(CF_UNICODETEXT))
			{
				HANDLE hClipboardData = GetClipboardData(CF_UNICODETEXT);
				GlobalUnlock(hClipboardData);
			}
		}
		ErrorMessage(hwnd, exCode);

	}
}

//������� ����
void CreateMenus(HWND hwnd)
{
	HMENU hMMenuBar, hMEdit, hMConfif;
	hMMenuBar = CreateMenu();
	hMEdit = CreateMenu();
	hMConfif = CreateMenu();
	hMConfigDelimiter = CreatePopupMenu();
	hMConfigMode = CreatePopupMenu();

	AppendMenu(hMMenuBar, MF_POPUP, (UINT_PTR)hMEdit, L"������");
	AppendMenu(hMEdit, MF_STRING, MA_COPY, L"����������");
	AppendMenu(hMEdit, MF_STRING, MA_PASTE, L"��������");
	AppendMenu(hMMenuBar, MF_POPUP, (UINT_PTR)hMConfif, L"���������");
	AppendMenu(hMConfif, MF_STRING | MF_POPUP, (UINT_PTR)hMConfigMode, L"����� ����������");
	AppendMenu(hMConfigMode, MF_STRING, MA_MODE_INTEGER, L"����� �����");
	AppendMenu(hMConfigMode, MF_STRING, MA_MODE_FRACTION, L"�����");
	AppendMenu(hMConfif, MF_STRING | MF_POPUP, (UINT_PTR)hMConfigDelimiter, L"�����������");
	AppendMenu(hMConfigDelimiter, MF_STRING, MA_DELIM_SLOOPING, L"/");
	AppendMenu(hMConfigDelimiter, MF_STRING, MA_DELIM_STRAIGHT, L"|");
	AppendMenu(hMMenuBar, MF_STRING, MA_HELP, L"&�������");

	SetMenu(hwnd, hMMenuBar);
}

void CreateControlPanel(HWND hwnd)
{
	//����� ��� ���� �������
	HWND hEditBack = CreateWindowEx(WS_EX_CLIENTEDGE,
		L"STATIC", L"",
		WS_VISIBLE | WS_CHILD | SS_RIGHT | SS_CENTERIMAGE | SS_BITMAP,
		10, 5, 460, 50,
		hwnd,
		NULL,
		GetModuleHandle(NULL),
		NULL);
	HANDLE hImg = LoadImageW(NULL, L"input.bmp", IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	SendMessageW(hEditBack, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImg);

	//�������
	hEdit = CreateWindowEx(WS_EX_CLIENTEDGE,
		L"STATIC", L"0",
		WS_VISIBLE | WS_CHILD | SS_RIGHT | SS_CENTERIMAGE ,
		10, 5, 460, 50,
		hwnd,
		NULL,
		GetModuleHandle(NULL),
		NULL);
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");
	SendMessage(hEdit, WM_SETFONT, WPARAM(hFont), TRUE);
	
	//����� ��� ���� ������� ������
	HWND hMemStatBack = CreateWindowEx(WS_EX_CLIENTEDGE,
		L"STATIC", L"",
		WS_VISIBLE | WS_CHILD | SS_RIGHT | SS_CENTERIMAGE | SS_BITMAP,
		10, 80, 60, 50,
		hwnd,
		NULL,
		GetModuleHandle(NULL),
		NULL);
	hImg = LoadImageW(NULL, L"input.bmp", IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	SendMessageW(hMemStatBack, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImg);
	
	//������ ������
	hMemoryStatus = CreateWindowEx(WS_EX_CLIENTEDGE,
		L"STATIC", L"",
		WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE,
		10, 80, 60, 50,
		hwnd,
		NULL,
		GetModuleHandle(NULL),
		NULL);

	//������ ���������� �������
	CreateToolTipForRect(CreateButton(hwnd, L"MC.bmp", 10, 140, 60, 50, (HMENU)IDM_MC ), L"�������� ������");
	CreateToolTipForRect(CreateButton(hwnd, L"MR.bmp", 10, 200, 60, 50, (HMENU)IDM_MR), L"���������� �� ������");
	CreateToolTipForRect(CreateButton(hwnd, L"MS.bmp", 10, 260, 60, 50, (HMENU)IDM_MS), L"��������� � ������");
	CreateToolTipForRect(CreateButton(hwnd, L"M+.bmp", 10, 320, 60, 50, (HMENU)IDM_MA), L"�������� � ����������� ������");

	//�������� ������
	CreateButton(hwnd, L"bckps.bmp", 105, 80, 60, 50, (HMENU)IDM_BACKSPASE);
	CreateToolTipForRect(CreateButton(hwnd, L"CE.bmp", 170, 80, 60, 50, (HMENU)IDM_CE), L"�������� ������������ ����� ������� ���������");
	CreateToolTipForRect(CreateButton(hwnd, L"C.bmp", 235, 80, 60, 50, (HMENU)IDM_C), L"������ ���������� ������ ���������");

	CreateButton(hwnd, L"7.bmp", 105, 140, 60, 50, (HMENU)IDM_7);
	CreateButton(hwnd, L"8.bmp", 170, 140, 60, 50, (HMENU)IDM_8);
	CreateButton(hwnd, L"9.bmp", 235, 140, 60, 50, (HMENU)IDM_9);

	CreateButton(hwnd, L"4.bmp", 105, 200, 60, 50, (HMENU)IDM_4);
	CreateButton(hwnd, L"5.bmp", 170, 200, 60, 50, (HMENU)IDM_5);
	CreateButton(hwnd, L"6.bmp", 235, 200, 60, 50, (HMENU)IDM_6);

	CreateButton(hwnd, L"1.bmp", 105, 260, 60, 50, (HMENU)IDM_1);
	CreateButton(hwnd, L"2.bmp", 170, 260, 60, 50, (HMENU)IDM_2);
	CreateButton(hwnd, L"3.bmp", 235, 260, 60, 50, (HMENU)IDM_3);

	CreateToolTipForRect(CreateButton(hwnd, L"sep.bmp", 105, 320, 60, 50, (HMENU)IDM_SEPARATE), L"�������� �����������");
	CreateButton(hwnd, L"0.bmp", 170, 320, 127, 50, (HMENU)IDM_0);

	//������ �������������� ��������
	CreateToolTipForRect(CreateButton(hwnd, L"neg.bmp", 340, 80, 60, 50, (HMENU)IDM_INVERSE), L"�������� ���� ������������� �����");
	CreateToolTipForRect(CreateButton(hwnd, L"sqr.bmp", 410, 80, 60, 50, (HMENU)IDM_SQR), L"���������� � �������");

	CreateButton(hwnd, L"div.bmp", 340, 140, 60, 50, (HMENU)IDM_DIVIDE);
	CreateToolTipForRect(CreateButton(hwnd, L"rev.bmp", 410, 140, 60, 50, (HMENU)IDM_REV), L"���������� ��������� ��������");

	CreateButton(hwnd, L"mul.bmp", 340, 200, 60, 50, (HMENU)IDM_MULTIPLY);


	CreateToolTipForRect(CreateButton(hwnd, L"=.bmp", 410, 200, 60, 170, (HMENU)IDM_COMPLETE), L"��������� ���������");

	CreateButton(hwnd, L"-.bmp", 340, 260, 60, 50, (HMENU)IDM_SUBTRACT);

	CreateButton(hwnd, L"+.bmp", 340, 320, 60, 50, (HMENU)IDM_ADD);
}

//������� ����������� ���������
void CreateToolTipForRect(HWND hwndParent, LPWSTR message)
{
	HWND hwndTT = CreateWindowEx(WS_EX_TOPMOST,
		TOOLTIPS_CLASS, NULL,
		WS_POPUP | TTS_NOPREFIX | TTS_ALWAYSTIP,
		CW_USEDEFAULT, CW_USEDEFAULT,
		CW_USEDEFAULT, CW_USEDEFAULT,
		hwndParent, NULL, gHInstance, NULL);

	SetWindowPos(hwndTT, HWND_TOPMOST,
		0, 0, 0, 0,
		SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
	TOOLINFO tooltip = { 0 };
#if (_WIN32_WINNT >= 0x0501)
	tooltip.cbSize = TTTOOLINFO_V1_SIZE;
#else
	tooltip.cbSize = sizeof(TOOLINFO);
#endif
	tooltip.uFlags = TTF_SUBCLASS;
	tooltip.hwnd = hwndParent;
	tooltip.hinst = gHInstance;
	tooltip.lpszText = message;
	GetClientRect(hwndParent, &tooltip.rect);
	SendMessage(hwndTT, TTM_ADDTOOL, 0, (LPARAM)(LPTOOLINFO)&tooltip);
}

//������� ������
HWND CreateButton(HWND hwnd, LPCWSTR name, int x, int y, int width, int height, HMENU hmenuID)
{
	HWND button = CreateWindowEx(NULL, L"BUTTON", L"", WS_VISIBLE | WS_CHILD | WS_TABSTOP | BS_BITMAP, x, y, width, height, hwnd, hmenuID, GetModuleHandle(NULL), NULL);
	HANDLE hImg = LoadImageW(NULL, name, IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR | LR_DEFAULTSIZE | LR_LOADFROMFILE);
	SendMessageW(button, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hImg);
	return button;
}

//��������� ���� �������
void ShowHelp(HWND hwnd)
{
	KWnd window(L"� ������������", L"about", gHInstance, SW_SHOW, AboutWndProc, NULL, CW_USEDEFAULT, CW_USEDEFAULT, 300, 150, CS_HREDRAW | CS_VREDRAW, WS_OVERLAPPEDWINDOW, NULL);
	MSG msg;	
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

//������� ��������� �������
LRESULT CALLBACK AboutWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CREATE:
	{
		HWND label = CreateWindowEx(WS_EX_CLIENTEDGE,
			L"STATIC", L"0",
			WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE,
			0, 0, 300, 150,
			hWnd,
			NULL,
			GetModuleHandle(NULL),
			NULL);
		HFONT hFont = CreateFont(30, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");
		SendMessage(hEdit, WM_SETFONT, WPARAM(hFont), TRUE);
		SetWindowText(label, L"������ ��������, 3100, ����");
		break;
	}	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}
