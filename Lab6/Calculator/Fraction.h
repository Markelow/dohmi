#ifndef FRACTION_H
#define FRACTION_H

#include "Enums.h"
#include <tchar.h>

#include <wchar.h>
#include <string>

using namespace std;

class Fraction
{
	public:
		Fraction();
		Fraction(long nominator, long denominator);
		Fraction(const wstring& fraction);
		void Validate(const wstring& fraction);
		Fraction operator + (Fraction const fraction);
		Fraction operator - (Fraction const fraction);
		Fraction operator * (Fraction const fraction);
		Fraction operator / (Fraction const fraction);
		Fraction Sqr();
		Fraction Rev();
		long FindLowestCommonMultiple(long firstDenominator, long secondDenominator);
		long GreatestCommonDivisor(long a, long b);
		void Compress(); 
		const wstring Fraction::ToString();
		bool IsZero();
		static const wchar_t getDelimiter() { return delimiter; }
		static FractionMode getMode() { return mode; }
		static void setMode(FractionMode value) { mode = value; }
		static SeparatorMode getSeparator() { return separator; }
		static void setSeparator(SeparatorMode value) 
		{ 
			separator = value; 
			delimiter = (slopingSM == separator) ? '/' : '|';
		}

	protected:
		static wchar_t delimiter;
		static FractionMode mode;
		static SeparatorMode separator;
		long nominator, denominator;
};

#endif