#include "Fraction.h"
#include "Enums.h"
#include <math.h>
#include "CalcErrors.h"
#include <regex>

//��������� �������������
FractionMode Fraction::mode = fractionFM;
wchar_t Fraction::delimiter = L'/';
SeparatorMode Fraction::separator = slopingSM;

Fraction::Fraction()
{
	this->denominator = 1;
	this->nominator = 1;
}

Fraction::Fraction(long nominator, long denominator)
{
	//���� �� ����� ���� � �����������
	if (0 == denominator)
	{
		throw 91;
	}
	//����� �� ���������� ������������� �����������
	if (denominator < 0)
	{
		denominator *= -1;
		nominator *= -1;
	}
	this->nominator = nominator;
	this->denominator = denominator;
}

//������� ����� �� ������ ������ � �� ��������������
Fraction::Fraction(const wstring& fraction)
{
	long nominator, denominator;
	//��������� �� ����������
	Validate(fraction);
	//���������, ���� �� �����������
	int delimiterPos = fraction.find(Fraction::getDelimiter());
	if (-1 == delimiterPos)
	{
		wchar_t delimiter = (L'/' == Fraction::getDelimiter()) ? L'|': L'/';
		delimiterPos = fraction.find(delimiter);
	}
	//��������� ��������
	nominator = (-1 == delimiterPos) ? _wtoi(fraction.c_str()) : _wtoi(fraction.substr(0, delimiterPos).c_str());
	denominator = (-1 == delimiterPos) ? 1 : _wtoi(fraction.substr(delimiterPos + 1).c_str());

	if (0 == denominator)
	{
		throw 91;
	}
	if (denominator < 0)
	{
		denominator *= -1;
		nominator *= -1;
	}
	this->nominator = nominator;
	this->denominator = denominator;
}

//��������� �����
void Fraction::Validate(const wstring& fraction)
{
	wregex wrx(L"^(-?)(\\d)+(([\/|\|](\\d)+)?)$");
	wsmatch results;
	if (!regex_match(fraction, results, wrx))
	{
		throw 97;
	}
}

//�������� ��������
Fraction Fraction::operator+(Fraction fraction)
{
	//���� ���������� ����� �������
	long lowestCommonDenominator = FindLowestCommonMultiple(this->denominator, fraction.denominator);
	//�������� � ������ ����������� ������ �����
	long factor = lowestCommonDenominator / this->denominator;
	long firstNominator = factor * this->nominator;
	//�������� ������
	factor = lowestCommonDenominator / fraction.denominator;
	long secondNominator = factor * fraction.nominator;
	//������� ����� �����
	Fraction result = Fraction(firstNominator + secondNominator, lowestCommonDenominator);
	//��������� ��
	result.Compress();
	return result;
}

//���� ���������� ����� �������
long Fraction::FindLowestCommonMultiple(long a, long b)
{
	return abs(a*b) / GreatestCommonDivisor(a, b);
}

//���� ���������� ����� ���������
long Fraction::GreatestCommonDivisor(long a, long b)
{
	if (0 == b)
	{
		return a;
	}
	else
	{
		return GreatestCommonDivisor(b, a % b);
	}
}

//��������� �����, ���� ��� ��������
void Fraction::Compress()
{
	//�����, �� ������� ���������
	long magicNumber;
	magicNumber = this->denominator;
	while (1 < magicNumber)
	{
		//���� �� ��� ����� ������� � ���������, � �����������
		while ((0 == this->nominator % magicNumber) && (0 == this->denominator % magicNumber))
		{
			this->nominator /= magicNumber;
			this->denominator /= magicNumber;
		}
		magicNumber--;
	} 
}

//�������� ���������
Fraction Fraction::operator - (Fraction fraction)
{
	long lowestCommonDenominator = FindLowestCommonMultiple(this->denominator, fraction.denominator);
	long factor = lowestCommonDenominator / this->denominator;
	long firstNominator = factor * this->nominator;
	factor = lowestCommonDenominator / fraction.denominator;
	long secondNominator = factor * fraction.nominator;
	Fraction result = Fraction(firstNominator - secondNominator, lowestCommonDenominator);
	result.Compress();
	return result;
}

//���������
Fraction Fraction::operator * (Fraction const fraction)
{
	Fraction result = Fraction(this->nominator * fraction.nominator, this->denominator * fraction.denominator);
	result.Compress();
	return result;
}

//�������
Fraction Fraction::operator / (Fraction const fraction)
{
	if (0 == fraction.nominator) { throw 92; }
	Fraction result = Fraction(this->nominator * fraction.denominator, this->denominator * fraction.nominator);
	result.Compress();
	return result;
}

//�������������� �����
Fraction Fraction::Rev()
{
	if (0 == this->nominator) { throw 93; }
	*this = Fraction(this->denominator, this->nominator);
	return *this;
}

//�������� ����� � �������
Fraction Fraction::Sqr()
{
	*this = Fraction(this->nominator * this->nominator, this->denominator*this->denominator);
	return *this;
}

//��������� ��������� ������������� �����
const wstring Fraction::ToString()
{
	wstring nomin, denomin;
	wstring wresult;
	long nominator = this->nominator;
	//�������� �����, ���� ����� ������������ 
	if (0 > nominator)
	{
		nominator *= -1;
		wresult.append(L"-");
		wresult += to_wstring(nominator);
	}
	wresult += to_wstring(nominator);
	//��������� �����������, ���� ����� ����� ��� � ������ ����� � ����������� 1
	if (!(integerFM == Fraction::mode && 1 == this->denominator))
	{
		wresult.append((*(L"/") == Fraction::getDelimiter()) ? L"/" : L"|");
		wresult += to_wstring(this->denominator);
	}
	return wresult;
}

bool Fraction::IsZero()
{
	return (0 == nominator);
}