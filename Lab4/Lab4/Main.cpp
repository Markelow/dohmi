#include <windows.h>
#include <math.h>
#include <tchar.h>
#include "KWnd.h"
#include <wchar.h>
#include <string> 

COLORREF startColor;
const int radius = 50;
POINT center;
const double PI = 3.141592;
const int step = 10;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
COLORREF ChoseColor();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG msg;
	KWnd mainWnd(L"Markelow 3100 Lab4", hInstance, nCmdShow, WndProc);

	while (GetMessage(&msg, NULL, 0, 0))  {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	HDC hDC;
	PAINTSTRUCT ps;
	switch (uMsg)
	{
		case WM_CREATE:
			startColor = ChoseColor();
			center.x = 100;
			center.y = 100;
			break;
		case WM_PAINT:
		{
			hDC = BeginPaint(hWnd, &ps);
			SelectObject(hDC, CreatePen(PS_SOLID, 1, startColor));
			const double rotation = 22.5;
			int factor;
			static POINT points[17];
			for (int i = 0; i < 17; ++i)
			{
				factor = (0 == i % 2) ? radius : radius - 20;
				points[i].x = center.x + factor * sin(rotation * i *PI / 180);
				points[i].y = center.y - factor * cos(rotation * i*PI / 180);
			}
			Polyline(hDC, points, sizeof(points) / sizeof(POINT));

			EndPaint(hWnd, &ps);
			break;
		}
		case WM_KEYDOWN:
		{
			RECT lp;
			GetClientRect(hWnd, &lp);
			if (VK_RIGHT == wParam)
			{
				center.x = (lp.right > center.x + step + radius) ? center.x + step : center.x;
			}
			if (VK_LEFT == wParam)
			{
				center.x = (lp.left  < center.x - radius - step) ? center.x - step : center.x;
			}
			InvalidateRect(hWnd, nullptr, true);
			break;
		}
		case WM_GETMINMAXINFO:
		{
			MINMAXINFO* mmi = reinterpret_cast<MINMAXINFO*>(lParam);
			mmi->ptMinTrackSize.x = 300;
			mmi->ptMinTrackSize.y = 200;
			break;
		}
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}
	return 0;
}

COLORREF ChoseColor()
{
	CHOOSECOLOR color;
	COLORREF ccref[16];
	COLORREF selcolor = 0x000000;

	memset(&color, 0, sizeof(color));
	color.lStructSize = sizeof(CHOOSECOLOR);
	color.hwndOwner = NULL;
	color.lpCustColors = ccref;
	color.rgbResult = selcolor;
	color.Flags = CC_RGBINIT;

	if (ChooseColor(&color))
	{
		selcolor = color.rgbResult;
	}
	return selcolor;
}

